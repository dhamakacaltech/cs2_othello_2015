#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
	Side mySide;
	Board othello_board;
    
    Move *doMove(Move *opponentsMove, int msLeft);
	vector<Move *> getMove(Board *board, Side side); 
	Move *minimax(int depth);
	double minimax_helper(Move* move, Side side, Board *board, int depth, double alpha, double beta);
	double get_score(Board *board, Move *move, Side side);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
