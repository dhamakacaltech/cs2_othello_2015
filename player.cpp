#include "player.h"
#include <vector>
#include "stdlib.h"
#include <stdio.h>
#include <math.h>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
	/*
	 * THanks for the free points
	 */
	 
	 othello_board = Board();
	 mySide = side;
	 
	
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}



std::vector<Move *> Player::getMove(Board *board, Side side) {
	std::vector<Move *> move_list;
	if(board->hasMoves(side))
	{
		for(int x = 0; x < 8; x++)
		{
			for(int y = 0; y < 8; y++)
			{
				Move *temp = new Move(x, y);
				if(board->checkMove(temp, side))
				{
					move_list.push_back(temp);
				}
				else{
				    delete temp;
				}
			}
		}
	}
	return move_list;
}



double Player::minimax_helper(Move* move, Side side, Board *board, int depth, double alpha, double beta){
    if(depth==0){
    	double score = get_score(board, move, side);
        return score;
    }
    board->doMove(move, side);
	if(board->isDone()){
		if(mySide == WHITE)
	    {
		    return -1*(board->countBlack() - board->countWhite());
	    }
	    else
	        return (board->countBlack() - board->countWhite());
	}
    Side newSide = (Side)(!side);
    std::vector<Move *> move_list = this->getMove(board, newSide);
    double max_score = -1;
    double a1 = alpha;
    double b1 = beta;
    for(unsigned int i = 0; i < move_list.size(); i++){
        Board *temp = board->copy();
        double temp_score = minimax_helper(move_list[i], newSide, temp, depth-1, a1, b1);
        delete temp;
        delete move_list[i];
        if(i==0){
            max_score = temp_score;
        }
        if(newSide==mySide){
            max_score = max(max_score, temp_score);
            if(temp_score > a1){
                a1 = temp_score;
            }
            if(a1 > beta){
                return beta;
            }
        }
        else{
            max_score = min(max_score, temp_score);
            if(temp_score < b1){
                b1 = temp_score;
            }
            if(b1 < alpha){
                return alpha;
            }
        }
    }
    return max_score;
}

Move *Player::minimax(int depth){
    std::vector<Move *> move_list = this->getMove(&othello_board, mySide);
    double max_score = -1000;
    Move *best_move = NULL;
    for(unsigned int i = 0; i < move_list.size(); i++){
        double temp_score = minimax_helper(move_list[i], mySide, othello_board.copy(), depth-1, -100000, 100000);
        if(temp_score > max_score){
            max_score = temp_score;
            best_move = move_list[i];
        }
    }
    return best_move;
}

double Player::get_score(Board *board, Move *move, Side side){

    double score = 0.0;
    double corner_weight = 300;
    double neg_corner_weight = -100;
    double edge_weight = 180;
    double amount_weight = 0.1;
    double color_mult = 1;
    if(mySide==WHITE){
        color_mult *= -1;
    }
    double moves_weight = 150;
    double side_mult = 1;
    if(side != mySide){
        side_mult *= -1;
        amount_weight = 1;
    }
    if((move->getX() == 0 || move->getX() == 7) && (move->getY() == 0 || move->getY() == 7)){
        score += side_mult*corner_weight;
    }
	else if(move->getX() == 0 || move->getX() == 7 || move->getY() == 0 || move->getY() == 7)
	{
	    if(move->getX() == 1 || move->getX() == 6 || move->getY() == 1 || move->getY() == 6){
	        score -= side_mult*neg_corner_weight;
	    }
	    else{
		    score += side_mult*edge_weight;
		}
	}
	else if((move->getX() == 1 || move->getX() == 6) && (move->getY() == 1 || move->getY() == 6)){
	    score -= side_mult*neg_corner_weight;
	}
	board->doMove(move, side);
	int diff = (board->countBlack() - board->countWhite());
	if(testingMinimax){
	    return color_mult*diff;
	}
	if(board->isDone()){
	    return color_mult*amount_weight*diff;
	}
	score += color_mult*amount_weight*diff;
	vector<Move *> myMoves = getMove(board, mySide);
	score += color_mult*moves_weight*myMoves.size();
    return score;
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
	 
	if (mySide == BLACK)
	{
		othello_board.doMove(opponentsMove, WHITE);
	}
	else
	{
		othello_board.doMove(opponentsMove, BLACK);
	}
	
	Board temp;
	if(testingMinimax){
	    Move *move = minimax(2);
	    othello_board.doMove(move, mySide);
	    return move;
	}
	std::vector<Move *> move_list = this->getMove(&othello_board, mySide);
	std::vector<double> move_score;
	if(move_list.size() == 0)
	{
		return NULL;
	}
	Move *move = minimax(4);
	othello_board.doMove(move, mySide);
	return move;
}




